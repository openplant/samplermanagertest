﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace OpenPlant
{
    public enum DataSourceType { None = 0, OPCDA = 1, ModbusTCP = 2 } //KEEP ADDING NEW DATASOURCE IN ENUM HERE
    public interface iDataSource
    {
        bool TryEnable(DataSourceConnection DataSourceConnectionParent); //this subroutine enables the XMLObject
        void Disable(); //This parameter disables the XMLObject
        void Update(iDataSource NewXMLObject); //This parameter updates the XMLObject if there are any property changed. It is run when
    }
    
    public class DataSourceBase
    {
        public DataSourceBase() { }     
        public string LogNode() { return "DSConn '" + _DataSourceConnectionParent.Name + "' > "; }
        protected DataSourceConnection _DataSourceConnectionParent = null;
    }

    public partial class DataSourceConnection : XMLFileScannableBase,  iXMLFileScannable
    {
        public DataSourceConnection() { }//Contructor - Parameterless constructor for XML Serialization

        string Node;
        DataSourceType _DataSourceType = DataSourceType.None;
        public DataSourceType DataSourceType { get { return _DataSourceType; } set { _DataSourceType = value; } }
        iDataSource CurrentDS;
        public bool TryEnable() //iXMLFileScannable Implementation
        {
            Node = "DSConn '" + this.Name + "' > ";
            Logger.Log(Node + "Initializing Data Source Connection", Global.Products["Sampler"].LogFilePath);
            CurrentDS = GetSelectedDS(this);
            if (CurrentDS != null) CurrentDS.TryEnable(this); else { Logger.Log(Node + "No Data Source configured/selected", Global.Products["Sampler"].LogFilePath); return false; }           
            return true;
        }

        iDataSource GetSelectedDS(DataSourceConnection DSC)
        {
            if (DSC.DataSourceType==DataSourceType.OPCDA) return (iDataSource)DSC.OPCDADataSource;
            else if (DSC.DataSourceType == DataSourceType.ModbusTCP) return (iDataSource)DSC.ModbusTCPDataSource;
            else return null;
        }

        public void Disable() //iXMLFileScannable Implementation
        {
            CurrentDS?.Disable();
        }
        
        public void Update(iXMLFileScannable NewDSConnection)
        {
            DataSourceConnection NewDSC = (DataSourceConnection)NewDSConnection;
            iDataSource NewSelectedDS = GetSelectedDS(NewDSC);
            if (NewSelectedDS != null) //Found a DataSource
            {
                if (NewSelectedDS.GetType() == CurrentDS.GetType()) CurrentDS.Update(NewSelectedDS); //If it is the Same DataSource
                else //If Different
                {
                    CurrentDS.Disable();
                    NewSelectedDS.TryEnable(this);
                }
            }
            else CurrentDS.Disable(); //No DataSource has IsSelected True;            
        }

        

        //******************************************** KEEP ADDING NEW DATA SOURCES HERE ***************************

        OPCDADataSource _OPCDADataSource = new OPCDADataSource(); public OPCDADataSource OPCDADataSource { get { return _OPCDADataSource; } set { _OPCDADataSource = value; } }
        ModbusTCPDataSource _ModbusTCPDataSource = new ModbusTCPDataSource(); public ModbusTCPDataSource ModbusTCPDataSource { get { return _ModbusTCPDataSource; } set { _ModbusTCPDataSource = value; } }
    }


    public class DataSourceTag
    {
        public DataSourceTag() {}
        bool _TagEnabled = true; public bool TagEnabled { get { return _TagEnabled; } set { _TagEnabled = value; } }

        public DateTime AddedOnUTC { get; set; } //The time when the tag was added in UTC Formats
        bool _ScalingRequired = false; public bool ScalingRequired { get { return _ScalingRequired; } set { _ScalingRequired = value; } }
        double _ScaleFactor_Range = 1; public double ScaleFactor_Range { get { return _ScaleFactor_Range; } set { _ScaleFactor_Range = value; } }
        double _ScaleFactor_Offset = 0; public double ScaleFactor_Offset { get { return _ScaleFactor_Offset; } set { _ScaleFactor_Offset = value; } }
        double _ScaleFactor_PostOffset = 0; public double ScaleFactor_PostOffset { get { return _ScaleFactor_PostOffset; } set { _ScaleFactor_PostOffset = value; } }

        bool _IsDuplicate = false;
        public bool IsDuplicate() { return _IsDuplicate; }
        public void SetIsDuplicate(bool IsDuplicate) { _IsDuplicate = IsDuplicate; }
    }
}
