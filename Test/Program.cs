﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenPlant;
using System.Diagnostics;
using System.Threading;
using System.Data.SQLite;
using System.Data;
using System.Collections;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {

            DataSourceConnection DSC = null;
            string ExStr = "";
            WCFClient<iSManagerContract> Client = new WCFClient<iSManagerContract>("127.0.0.1", 33174);
            Client.Start();
            Console.WriteLine("SAMPLER CONFIG CONTRACT TEST");


            DateTime ddd = Client.Channel.GetServerTimeUTC();
            Console.WriteLine(ddd);


            for (int n = 0; n < 2; n++)
            {
                if (Client.Channel.TryCreateNewDataSourceConnection(out DSC, out ExStr))
                {
                    Console.WriteLine("Successfully Created new DataSource '" + DSC.Name + "', FilePath on Sampler = '" + DSC.FilePath + "'");
                }
            }
            Console.WriteLine();


            List<string> DSNameList = new List<string>();
            if (Client.Channel.TryGetDataSourceConnectionNamesList(out DSNameList, out ExStr))
            {
                Console.WriteLine("Successfully obtained " + DSNameList.Count() + " Data Source Connection Names");
                foreach (string D in DSNameList)
                {
                    Console.WriteLine(D);
                }
            }
            Console.WriteLine();


            DataSourceConnection DSCOut;
            if (Client.Channel.TryGetDataSourceConnection(DSC.Name, out DSCOut, out ExStr))
            {
                Console.WriteLine("Successfully obtained Data Source Connection '" + DSCOut.Name +"'");
            }
            Console.WriteLine();

            
            if (Client.Channel.TryDeleteDataSourceConnection(DSC, out ExStr))
            {
                Console.WriteLine("Successfully deleted DataSource '" + DSC.Name + "', FilePath on Sampler = '" + DSC.FilePath + "'");
            }
            Console.WriteLine();


            //DO NOT TO USE THIS ONE AS IT MAY BE SLOW--------
            List<DataSourceConnection> DSCs = new List<DataSourceConnection>();
            if (Client.Channel.TryGetDataSourceConnections(out DSCs,out ExStr))
            {
                Console.WriteLine("Successfully obtained " + DSCs.Count() + " Data Source Connections");
                foreach (DataSourceConnection D in DSCs)
                {
                    Console.WriteLine(D.FilePath);
                }
            }
            Console.WriteLine();
            //-----------------------------------------------


            if (Client.Channel.TrySaveAndApplyDataSourceConnection(DSC, out ExStr))
            {
                Console.WriteLine("Successfully save Data Source Connection '" + DSC.Name +"', File Path on Sampler = '" + DSC.FilePath +"'");
            }
            Console.WriteLine();







            Console.ReadKey();
        }

    }    
}
